===== ISTRUZIONI =====

Il foglio LibreOffice Calc è pronto all'uso. Non modificare
la posizione di alcun file all'interno della cartella consegnata.

La funzione "Custom" permette di postare un tweet di lunghezza massima
140 caratteri e minima 1 carattere sull'account Twitter @Progetto2k15. Non è possibile postare di seguito
due tweet identici.

Credenziali di accesso account Twitter:
E-mail: progetto.progammazione2.2015@gmail.com
Password: rlacri93

===== NOTE =====

Il file di progetto NetBeans è assente perché tutto è
stato compilato usando editor testuali e la shell ubuntu.

La funzione "Complessa" che implementa il calcolo del determinante
di una matrice, fa uso della classe ausiliaria "Matrice Quadrata"
allo scopo di risolvere problemi di tipizzazione.
