package it.unica.pr2.progetto2015.g48886_49317_65115;
import it.unica.pr2.progetto2015.interfacce.*;

public class Complessa implements SheetFunction
{	
    /** 
    Argomenti in input ed output possono essere solo: String, Integer, Long, Double, Character, Boolean e array di questi tipi.
    Ad esempio a runtime si puo' avere, come elementi di args, un Integer ed un Long[], e restituire un Double[];
    */
    public Object execute(Object... args)
    {
		
		Integer c[][] = (Integer[][])args;
		
		int n = c.length;
		
		MatriceQuadrata mq = new MatriceQuadrata( n, c);

        return mq.determinante(c, n);

	}

    /** 
    Restituisce la categoria LibreOffice;
    Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
    */
    public final String getCategory()
    {
		return "Matrice";
	}

    /** Informazioni di aiuto */
    public final String getHelp()
    {
		return "Calcola il determinante di una matrice";
	} 

    /** 
    Nome della funzione.
    vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad es. "VAL.DISPARI" 
    */         
    public final String getName()
    {
		return "MATR.DETERM";
	}
}