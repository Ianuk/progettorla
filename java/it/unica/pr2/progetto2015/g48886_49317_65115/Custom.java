package it.unica.pr2.progetto2015.g48886_49317_65115;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import it.unica.pr2.progetto2015.interfacce.*;

public class Custom implements SheetFunction {


    public Object execute(Object... args){
		
        Twitter twitter = TwitterFactory.getSingleton(); //Stabilimento connessione a twitter
        
        String message=args[0].toString();
        
        if( message.length() > 140 ) //Controllo lunghezza massima messaggio 140 caratteri 
        {
            return (Object)"Errore: messaggio non valido"; //Se troppo lungo annulla e ritorna messaggio d'errore
        }
        Status status1;
        
        try
        {
            status1 = twitter.updateStatus(message);
        }
        
        catch( TwitterException te )
        {
            return (Object)"errore"; //In caso di TwitterException ritorna messaggio d'errore generico
        }
        
        return (Object)"status aggiornato con successo"; //Fine, ritorna messaggio di operazione conclusa

    }

    /** 
    Restituisce la categoria LibreOffice;
    Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
    */
    public final String getCategory() {
		return " ";
	}

    /** Informazioni di aiuto */
    public final String getHelp() {
		return " ";
	} 

    /** 
    Nome della funzione.
    vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad es. "VAL.DISPARI" 
    */         
    public final String getName() {
		return " ";
	}

}



