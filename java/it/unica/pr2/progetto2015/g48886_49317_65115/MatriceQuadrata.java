package it.unica.pr2.progetto2015.g48886_49317_65115;
import java.lang.Math;

public class MatriceQuadrata
{
	public Integer[][] matrix;
    int dim;

    MatriceQuadrata( int n, Integer[][] matrice )
    {
        dim=n;
        matrix=new Integer[n][n];
        matrix=matrice;
    }

    public int determinante(Integer a[][], int n)
    {
		int det = 0, sign = 1, p = 0, q = 0;				
	
		if( n == 1 ) 
		{
			det = a[0][0];												//Se la matrice ha ordine 1 allora il determinante e' il singolo elemento
		}
		
		else
		{
			Integer b[][] = new Integer[n-1][n-1];						//Istanziazione matrice ausiliaria
			for( int x = 0; x < n ; x++ )
			{
				p=0;													//Azzeramento pivot
				q=0;
				
				for( int i = 1; i < n; i++ )							//
				{														//
					for( int j = 0; j < n;j++ )							//
					{													//
						if( j != x )									//
						{												//
							b[p][q++] = a[i][j];						//Applicazione regola di Laplace
							if( q % (n-1) == 0 )						//
							{											//
								p++;									//
								q=0;									//
							}											//
						}												//
					}													//
				}														//
				
				det = det + a[0][x] * determinante(b, n-1) * sign;		//Chiamata ricorsiva, moltiplicazione determinante per determinante di matrice ridotta opportunamente cambiato di segno
				sign = -sign;											//Inversione segno
			}
		}
			return det;
	}
}